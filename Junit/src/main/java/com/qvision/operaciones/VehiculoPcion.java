package com.qvision.operaciones;

/**
 * Created by UserQV on 05/04/2017.
 */
public class VehiculoPcion {
    public String opcion(int opcion) {
        String veh = null;
        switch (opcion){
            case 1: veh="carro";
            break;
            case 2: veh="moto";
            break;
            case 3: veh="helicoptero";
            break;
            default: veh = "Invalido vehiculo";
            break;
        }
        return (veh);
    }
}
