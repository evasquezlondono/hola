package com.qvision.probar;

import com.qvision.operaciones.OperacionesAritmeticas;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by UserQV on 9/02/2017.
 */
public class OperacionesAritmeticasTest {

    @Test
    public void pruebaMultiplicacion(){
        int numUno = 5;
        int numDos = 2;
        //Metodo a llamar
        int resultadoMetodo = new OperacionesAritmeticas().multiplicacion(numUno,numDos);
        Assert.assertEquals(10, resultadoMetodo);
    }

    @Test
    public void pruebaSuma(){
        int numUno = 6;
        int numDos = 7;
        int resultadoSuma = new OperacionesAritmeticas().suma(numUno, numDos);
        Assert.assertEquals(13, resultadoSuma);
    }
}
