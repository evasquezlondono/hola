package com.qvision.operaciones;

/**
 * Created by UserQV on 9/02/2017.
 */
public class OperacionesAritmeticas {

    public int multiplicacion(int numUno, int numDos){
        return numUno * numDos;
    }

    public int suma(int numUno, int numDos){
        return numUno+numDos;
    }

}
