package com.qvision.probar;

/**
 * Created by UserQV on 05/04/2017.
 */

import static org.junit.Assert.*;
import com.qvision.operaciones.Operaciones;

        import org.junit.Test;

public class OperacionesTest {

    Operaciones operacion = new Operaciones();

    @Test
    public void testSumaNumerosPositivos() {
        assertEquals("1 + 2 debería ser = 3",3, operacion.suma(1,2));
    }

    @Test
    public void testSumaNumerosNegativos() {
        assertEquals("-1 + -3 debería ser = -3",-3, operacion.suma(-1,-2));
    }

    @Test
    public void testNumeroNegativoMasPositivo() {
        assertEquals("-1 + 5 debería ser = 4",4, operacion.suma(-1,5));
    }

/*	@Test
	public void testNumeroNegativoMasPositivoss() {
	Operaciones operacion = new Operaciones();
	assertNotNull(operacion.suma());
	}*/

}

