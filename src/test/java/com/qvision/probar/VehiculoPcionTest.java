package com.qvision.probar;

/**
 * Created by UserQV on 05/04/2017.
 */

import static org.junit.Assert.*;
import com.qvision.operaciones.VehiculoPcion;

import org.junit.Test;
public class VehiculoPcionTest {

    VehiculoPcion vehiculoopcion = new VehiculoPcion();

    @Test
    public void testOpcionCarro() {
        assertEquals("opcion 1 debería ser = carro","carro", vehiculoopcion.opcion(1));
    }

    @Test
    public void testOpcionMoto() {
        assertEquals("opcion 2 debería ser = moto","moto", vehiculoopcion.opcion(2));
    }

    @Test
    public void testOpcionHelicoptero() {
        assertEquals("opcion 2 debería ser = helicoptero","helicoptero", vehiculoopcion.opcion(3));
    }

}
